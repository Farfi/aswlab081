package springFront;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import models.ModelException;
import models.Tweet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import util.TimeService;


@Controller
public class JSONController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	
	@RequestMapping(value="/jsonexample", method=RequestMethod.GET, produces="application/json")
	@ResponseBody public Object homePageJson(HttpSession session, Model model) {
		Map<String,String> result = new HashMap<String,String>();
		result.put("status", "success");
		result.put("content", "It Works!");
		
	

		logger.info("Entering into the main page");

		String userName = (String) session.getAttribute("loggedUserNAME");
		Vector<HashMap<String, Object>> wallTweets = new Vector<HashMap<String, Object>>();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int deletable = 0;
		try {
			Collection<Tweet> tweets = Tweet.findAll();

			for (Tweet tweet : tweets) {
				HashMap<String,Object> tweetHash = new HashMap<String, Object>();
				tweetHash.put("tweetID", tweet.getTweet_id());
				tweetHash.put("authorID", tweet.getUser_id());
				tweetHash.put("content", tweet.getText());
				tweetHash.put("likes", tweet.getLikes());
				java.util.Date temps = mySQLTimeStamp.parse(tweet.getTime(), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				String authorName = tweet.getUser_name();
				tweetHash.put("authorName", authorName);
				if (userName !=null && userName.equals(authorName)) deletable++;
				wallTweets.addElement(tweetHash);
				
				
				result.put("tweetID", tweet.getTweet_id().toString());
				result.put("authorID", tweet.getUser_id().toString());
				result.put("content", tweet.getText());
				result.put("likes", tweet.getLikes().toString());
				result.put("tweetDate", TimeService.getDate(temps));
				result.put("tweetHour", TimeService.getHour(temps));
				result.put("authorName", authorName);

			}

			model.addAttribute("currentDate", TimeService.getCurrentDate());
			model.addAttribute("wallTweets",wallTweets);
			model.addAttribute("deletable",deletable);
			model.addAttribute("loggedUserNAME", userName);
			model.addAttribute("likedTable",session.getAttribute("likedTable"));

			logger.info("Rendering the main page");

			
			return result;
		}
		catch (ModelException ex) {
			model.addAttribute("theList",ex.getMessageList());
			return "error";
		}
	}

	
}
